package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLead extends ProjectMethods{

	public CreateLead() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.ID,using="createLeadForm_dataSourceId") WebElement eleSubmit;   
	
	@FindBy(how = How.NAME,using="submitButton") WebElement eleCompanyName;
	
	public CreateLead enterFirstname(String data) {
		
		clearAndType(eleFirstName, data);
		
		return this;
		
	}
	
	
public CreateLead enterLasttname(String data) {
		
		clearAndType(eleLastName, data);
		
		return this;
		
	}

public CreateLead enterCompanyname(String data) {
	
	clearAndType(eleCompanyName, data);
	
	return this;
	
}

public  ViewLeadPage clickCreateLead( ) {
	
	click(eleSubmit);
	
	return new ViewLeadPage();
	
}
	
}













